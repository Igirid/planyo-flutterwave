<?php
session_start();
// ini_set('display_errors', '1');
// ini_set('display_startup_errors', '1');
// error_reporting(E_ALL);

require __DIR__ . '/../vendor/autoload.php';


// print_r($_SESSION);

$reservation_id = $_SESSION['reservation_id'];
$reservation_id_hash = $_SESSION['reservation_id_hash'];
$security_key = $_SESSION['security_key'];
$security_key_hash = $_SESSION['security_key_hash'];
$amount = $_SESSION['amount'];
$currency = $_SESSION['currency'];
$callback_url = $_SESSION['callback_url'];
$redirect_url = $_SESSION['redirect_url'];

session_destroy();

// echo "Security key hash : $security_key_hash md5 :" . md5($security_key) . "\n";
// echo "reservation id hash : $reservation_id_hash md5 :" . md5($reservation_id) . "\n";

$transactionID = $_GET['transaction_id'];
$secret = env('FLW_SECRET_KEY');

$res = useCurl([], "https://api.flutterwave.com/v3/transactions/$transactionID/verify", null, [
    "Content-Type: application/json",
    "Authorization: Bearer $secret"
], false);

$response = json_decode($res[0], true);
$error = $res[1];

// print_r($response);

if ($response) {
    if (
        $response['data'] &&
        $response['data']['status'] === "successful"
        && intval($response['data']['amount']) >= $amount
        //[AMOUNT LIMIT] use this when flutterwave amount limit is lifted
        // && $amount >= $amount
        && $response['data']['currency'] === $currency
    ) {
        // Success! Confirm the customer's payment
        echo "<h4>Processing Payment...</h4>";
        $success = useCurl([
            'payment_status' => 'Completed',
            'reservation_id' => $reservation_id,
            'security_key' => $security_key,
            'amount' => $amount,
            'currency' => $currency,
            'transaction_id' => $transactionID

        ], $callback_url, 'POST');
        // print_r($success);
    } else {
        // Inform the customer their payment was unsuccessful
        // echo "inside else";
        echo "<h4>Invalid Payment...</h4>";

        $failed = useCurl([
            'payment_status' => 'Failed',
            'reservation_id' => $reservation_id,
            'security_key' => $security_key,
            'amount' => $amount,
            'currency' => $currency,
            'transaction_id' => $transactionID

        ], $callback_url, 'POST');

        // print_r($failed);
    }
    // echo "outside if";
}
echo "<form id='redirect' method='get' action='" . $redirect_url . "'>";
echo "<input type='hidden' name='meta[token]' value='54' />\n";
echo "<input type='submit' value='Submit' style='visibility: hidden;'>\n";
echo "</form>";

// useCurl([], $redirect_url, null, [], true, true);

?>
// <script type="text/javascript">
    document.forms[0].submit();
    // console.log(document.forms[0].outerHTML)
    // 
</script>