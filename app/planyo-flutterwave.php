<?php

session_unset();
session_start();
// error_reporting(E_ALL);

require __DIR__ . '/../vendor/autoload.php';


// Set session variables
$_SESSION["reservation_id"] = $_POST['reservation_id'];
// $_SESSION["amount"] = 35000;
$_SESSION["amount"] = intval($_POST['amount']);
$_SESSION["currency"] = $_POST['currency'];
$_SESSION["redirect_url"] = $_POST['redirect_url'];
$_SESSION["callback_url"] = $_POST['callback_url'];
$_SESSION["security_key"] = $_POST['security_key'];
$_SESSION["customer_id"] = $_POST['customer_id'];


$flutterwaveURL = "https://checkout.flutterwave.com/v3/hosted/pay";

$_SESSION["reservation_id_hash"] = md5($_SESSION["reservation_id"]);
$_SESSION["security_key_hash"] = md5($_SESSION["security_key"]);



?><html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <title>Planyo - Flutterwave payment proxy page.</title>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<body>
  <?php

  echo "<form id='flutterwave_form' method='post' action='" . $flutterwaveURL . "'>";
  foreach ($_POST as $item => $value) {
    //[AMOUNT LIMIT] remove this when flutterwave amount limit is lifted
    // if ($item == 'amount') {
    //   continue;
    // }
    if ($item == 'redirect_url') {
      continue;
    }
    echo "<input type='hidden' name='$item' value=\"$value\" />\n";
  }
  //   echo "<input type='hidden' name='requestFingerprintOrder' value='$requestFingerprintOrder' />\n";
  //   echo "<input type='hidden' name='requestFingerprint' value='$requestFingerprint' />\n";
  echo "<input type='hidden' name='public_key' value='" . env('PUBLIC_KEY') . "' />\n";
  // echo "<input type='hidden' name='public_key' value='FLWPUBK_TEST-SANDBOXDEMOKEY-X' />\n";
  echo "<input type='hidden' name='tx_ref' value='" . time() . $_POST['resource_name'] . $_POST['site_name'] . "' />\n";
  echo "<input type='hidden' name='redirect_url' value='" . env('Flutterwave_Redirect_URL') . "' />\n";
  echo "<input type='hidden' name='customer[name]' value='" . $_POST['first_name'] . ' '. $_POST['last_name'] . "' />\n";
  echo "<input type='hidden' name='customer[email]' value='" . $_POST['email'] . "' />\n";
  // echo "<input type='hidden' name='amount' value='2500' />\n";
  // echo "<input type='hidden' name='currency' value='NGN' />\n";
  // echo "<input type='hidden' name='meta[token]' value='54' />\n";
  echo "<input type='submit' value='Submit' style='visibility: hidden;'>\n";
  echo "</form>";

  ?>
  <script type="text/javascript">
    document.forms[0].submit();
    // console.log(document.forms[0].outerHTML)
  </script>
</body>

</html>