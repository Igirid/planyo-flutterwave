<?php

use Symfony\Component\Dotenv\Dotenv;


if (!function_exists('env')) {
    /**
     * get a value from .env file
     *
     * @param  string $value
     * @return mixed
     */
    function env($value)
    {

        $dotenv = new Dotenv();
        $dotenv->load(__DIR__ . '/../.env');
        return $_ENV[$value];
    }
}

if (!function_exists('useCurl')) {
    /**
     * send request with curl
     *
     * @param  string $value
     * @return mixed
     */

    function useCurl(array $data, string $url, $method = null, $headers = [], $echo = true, $redirect = false)
    {
        // dd("reached here");
        $ch = curl_init();
        // 173.230.149.104
        curl_setopt($ch, CURLOPT_URL, $url);
        // curl_setopt($ch, CURLOPT_URL, "https://10.2.100.111/TheVoice/api/v1/enquiry/get-user-account-details");
        if ($method && $method == 'POST') {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);  //Post Fields
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        if ($redirect) {
            # code...
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        }


        $error = null;

        $res = curl_exec($ch);
        // if (($res = curl_exec($ch)) === false) {
        //     $error = 'Curl error: ' . curl_error($ch);
        // } else {
        //     // $resp = json_decode($res, true);
        //     // if ($resp['ResponseCode'] == '00') {

        //     // } else {
        //     //     $error = $resp['ResponseMessage'];
        //     // }
        // }
        curl_close($ch);

        if (($method && $method == 'POST') || !$echo) {
            return [$res, $error];
        }

        echo $res;
    }
}
